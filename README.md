# FullstackDocker App 🚀

## Como funciona?

Este projeto tem como fins educativos em meu blog pessoal no Dev.to [Dev.to](https://dev.to/renanssantos1/zero-to-hero-como-criar-uma-aplicacao-fullstack-2g6h)!

### Como rodar o projeto

```
docker compose -f docker-compose-dev.yml up

```
### Como contribuir

Abra um fork desse repositorio, faça as devidas alterações, e solicite um Merge request.

